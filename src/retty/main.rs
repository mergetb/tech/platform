/// retty:
///     An all in one replacement for agetty and /bin/login for serial consoles. 
///     We've noticed this dynamic duo falling over often in the DComp testbed on 
///     the Minnowboard platform. Retty, which stands for `rust executed TTY` is an
///     attempt at a much simpler and more reliable serial console login daemon.

use std::{
    env, process, ptr, mem, fs,
    ffi::CString,
    fs::{OpenOptions, File},
    io::{self, Write, BufReader, prelude::*},
    os::unix::{fs::OpenOptionsExt, io::AsRawFd}
};
use nix::{
    sys::wait::wait, 
    unistd::{fork, ForkResult}
};
use anyhow::{Result, anyhow};
use errno::{Errno, errno, set_errno};
use log::{LevelFilter, info, error, debug};
use syslog::{Facility, Formatter3164, BasicLogger};

fn main() {

    info!("retty starting");

    match init_logging() {
        Ok(_) => {}
        Err(err) => { eprintln!("error: {}", err); process::exit(1) }
    }

    let dev = match process_args() {
        Ok(dev) => dev,
        Err(err) => { eprintln!("{}", err); process::exit(1) }
    };

    // Run the login as a child process we wait on. On a successful login the 
    // child process will execv into the shell of the user and that process
    // will ultimately end when the user exits thus ending the forked/exec'd
    // process as well. Then the parent process is here waiting to start the
    // login prompt again.
    loop { match fork() {

        Ok(ForkResult::Parent { child: _, .. }) => { 
            match wait() {
                Ok(_) => { continue; }
                Err(err) => { eprintln!("wait failed: {}", err); }
            }
        }

        Ok(ForkResult::Child) => {
            process::exit(
                match run_tty(&dev) {
                    Ok(_) => 0,
                    Err(err) => { eprintln!("error: {}", err); 1 }
                }
            );
        }

        Err(_) => eprintln!("fork failed")

    }}

}

fn run_tty(tty: &String) -> Result<()> {

    set_process_controlling_terminal(tty)?;
    setup_tty_io(tty)?;
    configure_tty(tty)?;
    do_login()?;

    Ok(())

}

fn do_login() -> Result<()> {

    // clear the line
    println!("");

    // ensure the tty is in blocking-io mode
    unsafe {
        libc::fcntl(
            libc::STDOUT_FILENO, 
            libc::F_SETFL,
            libc::fcntl(libc::STDOUT_FILENO, libc::F_GETFL, 0) & !libc::O_NONBLOCK);
    }

    // standard username password login prompts
    print!("user: ");
    io::stdout().flush()?;
    let mut user = String::new();
    io::stdin().read_line(&mut user)?;
    let username = &user.trim_end().to_string();

    // support slapping enter to scroll, very useful on a new console connection
    if username.is_empty() {
        return Ok(())
    }

    let pass = rpassword::prompt_password_stdout("Password: ")?;

    // get the users shadow entry
    let ent = getent(username)?;

    match pwhash::unix::verify(pass, &ent[..]) {
        true => run_user_shell(username),
        false => Err(anyhow!("bad username or password")),
    }

}

fn run_user_shell(username: &String ) -> Result<()> {

    // Leverage su to do a number of the right things, including allocating a 
    // controlling proces PTY
    let argv: Vec<&str> = vec!["su", "-", &username[..]];

    let cargv: Vec<_> = argv.iter()
        .map(|x| CString::new(*x).unwrap())
        .collect();

    let mut pargv: Vec<_> = cargv.iter()
        .map(|x| x.as_ptr())
        .collect();

    pargv.push(ptr::null());

    unsafe {

        // ensure we are in blocking-io mode
        libc::fcntl(
            libc::STDOUT_FILENO, 
            libc::F_SETFL,
            libc::fcntl(libc::STDOUT_FILENO, libc::F_GETFL, 0) & !libc::O_NONBLOCK);

        // dont murder filde descriptors on exec
        libc::fcntl(
            libc::STDOUT_FILENO, 
            libc::F_SETFD,
            libc::fcntl(libc::STDOUT_FILENO, libc::F_GETFD, 0) & !libc::FD_CLOEXEC);

        // jump into the users shell
        set_errno(Errno(0));
        libc::execvp(pargv[0], pargv.as_ptr());

    }

    // should never get here
    return Err(anyhow!("execv failed: {}", errno()))

}


fn getent(user: &String) -> Result<String> {

    let f = File::open("/etc/shadow")?;
    let mut reader = BufReader::new(f);

    loop {
        let mut line = String::new();
        match reader.read_line(&mut line) {
            Ok(0) => { break; }
            Ok(_) => {
                if line.starts_with(user) {
                    let parts : Vec<&str> = line.split(':').collect();
                    return Ok(parts[1].to_string())
                }
            }
            Err(x) => { return Err(anyhow!("{}", x)) }

        }
    }

    Err(anyhow!("bad username or password"))

}

fn get_tty_file(tty: &String) -> Result<File> {

    let file = OpenOptions::new()
                .read(true)
                .write(true)
                .custom_flags(libc::O_NOCTTY|libc::O_NONBLOCK)
                .open(tty)?;

    unsafe {
        if libc::isatty(file.as_raw_fd()) != 1 {
            return Err(anyhow!("{} is not a tty", tty));
        }
    }

    Ok(file)

}

fn set_process_controlling_terminal(tty: &String) -> Result<()> {
        
    let ttyf = get_tty_file(tty)?;

    unsafe {
        let fd = ttyf.as_raw_fd();
        let tid = libc::tcgetsid(fd);
        if tid < 0 || process::id() != tid as u32 {
            set_errno(Errno(0));
            if libc::ioctl(fd, libc::TIOCSCTTY, 1) == -1 {
                debug!("failed to set process controlling terminal: {}", errno());
            }
        }
    }

    Ok(())

}


fn setup_tty_io(tty: &String) -> Result<()> {

    extern {
        static stdout: *mut libc::FILE;
    }

    unsafe {

        // close current stdin and setup the provided tty as stdin
        libc::close(libc::STDIN_FILENO);
        let ctty = CString::new(&tty[..]).expect("CString::new failed").into_raw();
        if libc::open(ctty, libc::O_RDWR|libc::O_NOCTTY|libc::O_NONBLOCK, 0) != 0 {
            return Err(anyhow!("failed to open tty as stdin"));
        }


        // close current stdout dup tty fd to stdout
        libc::close(libc::STDOUT_FILENO);
        let rc = libc::dup(libc::STDIN_FILENO);
        if rc != 1 {
            return Err(anyhow!("failed to setup stdout: {}", rc))
        }

        // close current stderr dup tty fd to stderr
        libc::close(libc::STDERR_FILENO);
        let rc = libc::dup(libc::STDIN_FILENO);
        if rc != 2 {
            return Err(anyhow!("failed to setup stderr: {}", rc))
        }

        libc::setvbuf(stdout, ptr::null_mut::<i8>(), libc::_IONBF, 0);

        set_errno(Errno(0));
        if libc::login_tty(libc::STDIN_FILENO) != 0 {
            return Err(anyhow!("failed to prepare tty for login: {}", errno()));
        }

    }

    Ok(())

}


fn configure_tty(tty: &String) -> Result<()> {

    macro_rules! ctrl {
        ( $x:expr ) => { $x as u8 & 0x1f }
    }

    let baud = determine_baud(tty)?;

    use libc::{
        // special characters
        VINTR, VQUIT, VERASE, VKILL, VEOF, VSTART, VSTOP, VSUSP, VREPRINT, VWERASE, VLNEXT, VDISCARD,

        // special settings
        VMIN, VTIME,

        // control settings
        CS8, HUPCL, CREAD,

        // input settings
        ICRNL, IUTF8,

        // output settings
        OPOST, ONLCR, NL0, CR0, TAB0, BS0, VT0, FF0,

        // local settings
        ISIG, ICANON, IEXTEN, ECHO, ECHOE, ECHOK, ECHOKE
    };

    unsafe {

        let mut tp : libc::termios = mem::zeroed();

        tp.c_ispeed = baud;
        tp.c_ospeed = baud;

        // special characters
        tp.c_cc[VINTR] =    ctrl!('c');
        tp.c_cc[VQUIT] =    ctrl!('\\');
        tp.c_cc[VERASE] =   ctrl!('h');
        tp.c_cc[VKILL] =    ctrl!('u');
        tp.c_cc[VEOF] =     ctrl!('d');
        tp.c_cc[VSTART] =   ctrl!('q');
        tp.c_cc[VSTOP] =    ctrl!('s');
        tp.c_cc[VSUSP] =    ctrl!('z');
        tp.c_cc[VREPRINT] = ctrl!('r');
        tp.c_cc[VWERASE] =  ctrl!('?');
        tp.c_cc[VLNEXT] =   ctrl!('v');
        tp.c_cc[VDISCARD] = ctrl!('d');

        // speical settings
        tp.c_cc[VMIN]  = 1;
        tp.c_cc[VTIME] = 0;


        // conrol settings
        tp.c_cflag = CS8 | HUPCL | CREAD;

        // input settings
        tp.c_iflag = ICRNL | IUTF8;

        // output settings
        tp.c_oflag = OPOST | ONLCR | NL0 | CR0 | TAB0 | BS0 | VT0 | FF0;

        // local settings
        tp.c_lflag = ISIG | ICANON | IEXTEN | ECHO | ECHOE | ECHOK | ECHOKE;

        set_errno(Errno(0));
        if libc::cfsetspeed(&mut tp, baud) < 0 {
            error!("failed to configure tty: {}", errno());
        }

        set_errno(Errno(0));
        if libc::tcsetattr(libc::STDIN_FILENO, libc::TCSANOW, &tp) < 0 {
            error!("failed to configure tty: {}", errno());
        }

    }

    Ok(())

}

fn determine_baud(tty: &String) -> Result<u32> {

    // default to 9600
    let mut baud = libc::B9600;

    match tty.split("/").last() {
        Some(ttydev) =>  {
            let prefix = format!("console={}", ttydev);

            let cmdline = fs::read_to_string("/proc/cmdline")?;
            for arg in cmdline.split_whitespace() {

                if arg.starts_with(prefix.as_str()) {

                    for param in arg.split(",") {
                        if      param.starts_with("19200")  { baud = libc::B19200;  }
                        else if param.starts_with("38400")  { baud = libc::B38400;  }
                        else if param.starts_with("57600")  { baud = libc::B57600;  }
                        else if param.starts_with("115200") { baud = libc::B115200; }
                    }

                }

            }
        }
        None => {}
    }

    Ok(baud)
}

fn init_logging() -> Result<()> {

    let formatter = Formatter3164 {
        facility: Facility::LOG_USER,
        hostname: None,
        process: "retty".into(),
        pid: 0,
    };

    let logger = syslog::unix(formatter).expect("could not connect to syslog");
    log::set_boxed_logger(Box::new(BasicLogger::new(logger)))
            .map(|()| log::set_max_level(LevelFilter::Info))?;

    Ok(())

}

fn process_args() -> Result<String> {

    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        return Err(anyhow!("usage: retty <dev>"))
    }

    Ok(args[1].clone())

}
