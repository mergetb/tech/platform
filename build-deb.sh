#!/bin/bash

mkdir -p build

rm -f build/*.build*
rm -f build/*.change
rm -f build/*.deb

debuild \
    -e V=1 -e prefix=/usr -e arch=amd64 \
    $DEBUILD_ARGS -aamd64 -i -us -uc -b

mv ../*.build* build/
mv ../*.changes build/
mv ../*.deb build/
