# Platform

This repository contains programs and tools for hardware platforms that run in
Merge.

## [retty](src/retty)

An all in one replacement for
[agetty](https://www.tldp.org/HOWTO/Remote-Serial-Console-HOWTO/getty-agetty.html)
and `/bin/login` for serial consoles. We've noticed this dynamic duo falling
over often in the DComp testbed on the Minnowboard platform. Retty, which stands
for `rust executed TTY` is an attempt at a much simpler and more reliable serial
console login daemon.

