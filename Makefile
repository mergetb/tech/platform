prefix ?= /usr/local

.PHONY: all
all: target/debug/retty target/release/retty

target/debug/retty: src/retty/main.rs
	cargo build

target/release/retty: src/retty/main.rs
	cargo build --release

clean:
	rm -rf target

install: target/release/retty
	install -D target/release/retty $(DESTDIR)$(prefix)/bin/retty
