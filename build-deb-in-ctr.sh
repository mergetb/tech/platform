#!/bin/bash

set -e

docker build $BUILD_ARGS -f debian/builder.dock -t platform-builder .
docker run -v `pwd`:/platform platform-builder /platform/build-deb.sh

