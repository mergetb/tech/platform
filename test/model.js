topo = {
    name: 'rtty',
    nodes: [
        { 
            name: 'ry',
            image: 'debian-bullseye',
            mounts: [{ source: env.PWD+'/..', point: '/rtty' }]
        },
    ],
}
